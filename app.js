var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose(); //add verbose for long stack traces
var SmsGateway = require('./public/js/smsgateway');
var socket_io = require('socket.io');
var colors = require('colors');

var smswall = require('./routes/smswall');
var clublied = require('./routes/clublied');

// Express
var app = express();

// Socket.io
var io = socket_io();
app.io = io;

// config inladen
// Lokaal:
//var config = require('config');
//var smsgateway_username = config.get('smsgateway.username');
//var smsgateway_password = config.get('smsgateway.password');
// Heroku:
var smsgateway_username = process.env.USERNAME;
var smsgateway_password = process.env.PASSWORD;

//smsgateway inloggen
var gateway = new SmsGateway(smsgateway_username, smsgateway_password);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'images/favicon.ico')));
//app.use(logger('dev')); // Om GET e.d. te loggen
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', smswall);
app.use('/clublied', clublied);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//Constanten hardcoden
var interval_fillDatabase = 2.5; //seconden
var interval_updateClient = 2.5; //seconden
var smsen_per_pagina = 15;
var start_timeStamp = Math.floor((new Date).getTime() / 1000);

//Socket IO functions
var stopcontact = null; //om te kunnen emitten van de ander functies
io.sockets.on('connection', function(socket) {
    stopcontact = socket;
    //Laatste 1O smsen direct doorsturen zodat het scherm vol is na refresh
    var db = new sqlite3.Database('sqlite.db');
    db.configure("busyTimeout", 60000);
    db.each("SELECT message FROM (SELECT message, created_at FROM messages WHERE displayed is 'true' ORDER BY created_at DESC Limit " + smsen_per_pagina + ") ORDER BY created_at ASC; ", function(err, row) {
        pushMessage(stopcontact, row.message);
    });
    db.close();
}); //einde van io.on(connection)

//On disconnect of socket
io.sockets.on('disconnect', function() {
    stopcontact = null;
});

// Database checken en nieuwe berichten pushen
function updateClient() {
    //console.log('[pushMessage]');
    var db = new sqlite3.Database('sqlite.db');
    db.configure("busyTimeout", 60000);
    db.each("SELECT gateway_id, message FROM messages WHERE displayed is 'false' ORDER BY created_at ASC Limit 10", function(err, row) {
        pushMessage(stopcontact, row.message, row.gateway_id);
        // console.log(row);
    });
    db.close();
    setTimeout(fillDatabase, interval_fillDatabase * 1000);
}

// SMSgateway checken en nieuwe berichten inladen
function fillDatabase() {
    //tellen hoeveel berichten we nu al hebben, zodat welke pagina we moeten opvragen van smsgateway
    //console.log('[fillDatabase]');
    var db = new sqlite3.Database('sqlite.db');
    db.configure("busyTimeout", 60000);
    db.get("SELECT COUNT(*) as count FROM messages", function(err, row) {
        var pagina = Math.ceil((row.count + 1) / 500);
        // console.log("Pagina:"+pagina);
        fillDatabaseWithPage(pagina);
    });
    db.close();
    setTimeout(updateClient, interval_updateClient * 1000);
}
fillDatabase();

// Stuur een SMS door naar de client
function pushMessage(socket, sms, gateway_id) {
    //console.log('[pushMessage]');
    if (socket != null) {
        if (gateway_id != null) {
            logmessage = 'Pushed new SMS - ' + gateway_id + ' - ' + sms;
            console.log(logmessage.yellow);
        } else {
            logmessage = 'Pushed to refresh page - ' + sms;
            console.log(logmessage.grey);
        }
        updateDisplayStatusInDatabase(gateway_id, 'true');
        socket.emit('newMessage', {
            sms: sms
        });
    }
}

//Database met specifieke pagina updaten
function fillDatabaseWithPage(page) {
    //console.log('[fillDatabaseWithPage:'+page+']');
    gateway.getMessages(page).then(function(data) {
        laatsteBerichtenPagina = data;
        //console.log("SMSEN op page "+page+":"+laatsteBerichtenPagina.length);
        for (messages = 0; messages < laatsteBerichtenPagina.length; messages++) {
            var gateway_id = laatsteBerichtenPagina[messages].id;
            var bericht = laatsteBerichtenPagina[messages].message;
            var status = laatsteBerichtenPagina[messages].status;
            var phonenumber = laatsteBerichtenPagina[messages].contact.number;
            var created_at = laatsteBerichtenPagina[messages].created_at;
            var displayed = 'false';
            var epoch = Math.floor((new Date).getTime() / 1000);
            if (created_at > start_timeStamp + 3600) { //in het laatste uur
                insertMessageIntoDatabase(gateway_id, bericht, status, phonenumber, epoch, displayed);
            }
        }
    }).fail(function(message) {
        console.log('failed', message);
    });
}

//een bericht in de database steken of updaten
function insertMessageIntoDatabase(gateway_id, bericht, status, phonenumber, created_at, displayed) {
    var db = new sqlite3.Database('sqlite.db');
    //console.log(decodeHtmlEntities(bericht));
    db.configure("busyTimeout", 60000);
    db.run("INSERT OR IGNORE INTO messages (gateway_id , message, status, phonenumber, created_at, displayed) VALUES ($gateway_id, $message, $status, $phonenumber, $created_at, $displayed)", {
        $gateway_id: gateway_id,
        $message: decodeHtmlEntities(bericht),
        $status: status,
        $phonenumber: correctPhonenumber(phonenumber),
        $created_at: created_at,
        $displayed: displayed
    });
    db.close();
}

// displaystatus van een bericht in database updaten
function updateDisplayStatusInDatabase(gateway_id, newStatus) {
    var db = new sqlite3.Database('sqlite.db');
    db.configure("busyTimeout", 60000);
    //console.log("MESSAGE_UPDATE:"+gateway_id);
    db.run("UPDATE messages SET displayed=$status WHERE gateway_id=$gateway_id", {
        $gateway_id: gateway_id,
        $status: newStatus,
    });
    db.close();
}

//phonenumberconversie: 00 -> +
function correctPhonenumber(phonenumber) {
    //console.log("correctPhonenumber: "+phonenumber);
    if (phonenumber.length > 1 && phonenumber.substring(0, 2) == "00") {
        return '+' + phonenumber.substring(2, phonenumber.length - 1);
    } else {
        return phonenumber;
    }
}

var decodeHtmlEntities = function(str) {
    return str.replace(/&#?(\w+);/g, function(match, dec) {
        if (isNaN(dec)) {
            chars = {
                quot: 34,
                amp: 38,
                lt: 60,
                gt: 62,
                nbsp: 160,
                copy: 169,
                reg: 174,
                deg: 176,
                frasl: 47,
                trade: 8482,
                euro: 8364,
                Agrave: 192,
                Aacute: 193,
                Acirc: 194,
                Atilde: 195,
                Auml: 196,
                Aring: 197,
                AElig: 198,
                Ccedil: 199,
                Egrave: 200,
                Eacute: 201,
                Ecirc: 202,
                Euml: 203,
                Igrave: 204,
                Iacute: 205,
                Icirc: 206,
                Iuml: 207,
                ETH: 208,
                Ntilde: 209,
                Ograve: 210,
                Oacute: 211,
                Ocirc: 212,
                Otilde: 213,
                Ouml: 214,
                times: 215,
                Oslash: 216,
                Ugrave: 217,
                Uacute: 218,
                Ucirc: 219,
                Uuml: 220,
                Yacute: 221,
                THORN: 222,
                szlig: 223,
                agrave: 224,
                aacute: 225,
                acirc: 226,
                atilde: 227,
                auml: 228,
                aring: 229,
                aelig: 230,
                ccedil: 231,
                egrave: 232,
                eacute: 233,
                ecirc: 234,
                euml: 235,
                igrave: 236,
                iacute: 237,
                icirc: 238,
                iuml: 239,
                eth: 240,
                ntilde: 241,
                ograve: 242,
                oacute: 243,
                ocirc: 244,
                otilde: 245,
                ouml: 246,
                divide: 247,
                oslash: 248,
                ugrave: 249,
                uacute: 250,
                ucirc: 251,
                uuml: 252,
                yacute: 253,
                thorn: 254,
                yuml: 255,
                lsquo: 8216,
                rsquo: 8217,
                sbquo: 8218,
                ldquo: 8220,
                rdquo: 8221,
                bdquo: 8222,
                dagger: 8224,
                Dagger: 8225,
                permil: 8240,
                lsaquo: 8249,
                rsaquo: 8250,
                spades: 9824,
                clubs: 9827,
                hearts: 9829,
                diams: 9830,
                oline: 8254,
                larr: 8592,
                uarr: 8593,
                rarr: 8594,
                darr: 8595,
                hellip: 133,
                ndash: 150,
                mdash: 151,
                iexcl: 161,
                cent: 162,
                pound: 163,
                curren: 164,
                yen: 165,
                brvbar: 166,
                brkbar: 166,
                sect: 167,
                uml: 168,
                die: 168,
                ordf: 170,
                laquo: 171,
                not: 172,
                shy: 173,
                macr: 175,
                hibar: 175,
                plusmn: 177,
                sup2: 178,
                sup3: 179,
                acute: 180,
                micro: 181,
                para: 182,
                middot: 183,
                cedil: 184,
                sup1: 185,
                ordm: 186,
                raquo: 187,
                frac14: 188,
                frac12: 189,
                frac34: 190,
                iquest: 191,
                Alpha: 913,
                alpha: 945,
                Beta: 914,
                beta: 946,
                Gamma: 915,
                gamma: 947,
                Delta: 916,
                delta: 948,
                Epsilon: 917,
                epsilon: 949,
                Zeta: 918,
                zeta: 950,
                Eta: 919,
                eta: 951,
                Theta: 920,
                theta: 952,
                Iota: 921,
                iota: 953,
                Kappa: 922,
                kappa: 954,
                Lambda: 923,
                lambda: 955,
                Mu: 924,
                mu: 956,
                Nu: 925,
                nu: 957,
                Xi: 926,
                xi: 958,
                Omicron: 927,
                omicron: 959,
                Pi: 928,
                pi: 960,
                Rho: 929,
                rho: 961,
                Sigma: 931,
                sigma: 963,
                Tau: 932,
                tau: 964,
                Upsilon: 933,
                upsilon: 965,
                Phi: 934,
                phi: 966,
                Chi: 935,
                chi: 967,
                Psi: 936,
                psi: 968,
                Omega: 937,
                omega: 969
            }
            if (chars[dec] !== undefined) {
                dec = chars[dec];
            }
        }
        return String.fromCharCode(dec);
    });
};

module.exports = app;
